#include "MyClient.h"
#include "MyLogger.h"

#define F (char*)__FILE__
#define L __LINE__
MyClient::MyClient()
	: is_initialized(false), ip("127.0.0.1"), port(5004), 
	img_convert_ctx(NULL), context(NULL), ccontext(NULL),
	src_buf(NULL), src_picture(NULL), dst_buf(NULL), dst_picture(NULL)
{}

MyClient::~MyClient()
{
	if (this->is_initialized)
		Deinitialize();
}

bool MyClient::Initialize(std::string ip, int port)
{
	int ret;
	AVDictionary *inputOptionsDict = nullptr;
	char BUFFLENGTH[15];
	context = avformat_alloc_context();
	ccontext = avcodec_alloc_context3(NULL);
	av_init_packet(&packet);

	/* Initialize libavcodec, and register all codecs and formats. */
	av_register_all();
	avformat_network_init();

	std::string tempUrl("");
	tempUrl.append("rtp://");
	tempUrl.append(ip + ":");
	tempUrl.append(std::to_string(port));
	sprintf(BUFFLENGTH, "%d", BUFFSIZEOFRTP);
	//av_dict_set(&inputOptionsDict, "rtsp_transport", "tcp", 0); rtsp��
	av_dict_set(&inputOptionsDict, "reorder_queue_size", "0", 0);
	av_dict_set(&inputOptionsDict, "buffer_size", BUFFLENGTH, 0);
	/* allocate the media context */
	if (avformat_open_input(&context, tempUrl.c_str(), NULL, &inputOptionsDict) != 0) {
		this->last_error = MyClientError::CANT_ALLOC_FORMAT_CONTEXT;
		return false;
	} 
	if (avformat_find_stream_info(context, NULL) < 0) {
		this->last_error = MyClientError::CANT_FIND_STREAM_INFO;
		return false;
	}

	//search video stream
	for (int i = 0; i < context->nb_streams; i++) {
		if (context->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
			video_stream_index = i;
	}

	// set codec
	AVCodec *codec = NULL;
	codec = avcodec_find_decoder(context->streams[video_stream_index]->codec->codec_id);
	if (!codec) {
		return false;
	}

	avcodec_get_context_defaults3(ccontext, codec);
	avcodec_copy_context(ccontext, context->streams[video_stream_index]->codec);


	if (avcodec_open2(ccontext, codec, NULL) < 0) {
		this->last_error = MyClientError::CANT_OPEN_CODEC;
		return false;
	}
	ccontext->width = 1920;
	ccontext->height = 1080;
	img_convert_ctx = sws_getContext(ccontext->width, ccontext->height, ccontext->pix_fmt,
		ccontext->width, ccontext->height, AV_PIX_FMT_BGR24,
		SWS_BICUBIC, NULL, NULL, NULL);

	int size = avpicture_get_size(ccontext->pix_fmt, ccontext->width, ccontext->height);
	src_buf = (uint8_t*)(av_malloc(size));
	src_picture = av_frame_alloc();
	int size2 = avpicture_get_size(AV_PIX_FMT_BGR24, ccontext->width, ccontext->height);
	dst_buf = (uint8_t*)(av_malloc(size2));
	dst_picture = av_frame_alloc();
	avpicture_fill((AVPicture *)src_picture, src_buf, ccontext->pix_fmt, ccontext->width, ccontext->height);
	avpicture_fill((AVPicture *)dst_picture, dst_buf, AV_PIX_FMT_BGR24, ccontext->width, ccontext->height);

	this->is_initialized = true;

	return true;
}

void MyClient::Deinitialize()
{
	av_free_packet(&packet);
	if (src_buf != NULL)
	{
		av_free(src_buf);
		src_buf = NULL;
	}
	if (dst_buf != NULL)
	{
		av_free(dst_buf);
		dst_buf = NULL;
	}
	if (dst_picture != NULL)
	{
		av_free(dst_picture);
		dst_picture = NULL;
	}
	if (src_picture != NULL)
	{
		av_free(src_picture);
		src_picture = NULL;
	}
	avformat_free_context(context);
	avcodec_free_context(&ccontext);
	av_read_pause(context);

	this->is_initialized = false;
}

bool MyClient::ReceiveImage(cv::Mat& cv_img)
{
	static AVFrame* frame;

	if (av_read_frame(context, &packet) >= 0)
	{
		int check = 0;
		int result = avcodec_decode_video2(ccontext, src_picture, &check, &packet);

		if (!check)
		{
			this->last_error = MyClientError::CANT_DECODE_IMAGE;
			return false;
		}

		sws_scale(img_convert_ctx, src_picture->data, src_picture->linesize, 0, ccontext->height, dst_picture->data, dst_picture->linesize);
		cv::Mat mat(ccontext->height, ccontext->width, CV_8UC3, dst_picture->data[0], dst_picture->linesize[0]);
		cv_img = mat.clone();

		
		return true;
	}
	else
		return false;
}


bool MyClient::IsInitialized()
{
	return this->is_initialized;
}


bool MyClient::StartReceive()
{
	//EndReceive();

	mtx_lock.lock();
	this->is_receiving = true;
	mtx_lock.unlock();
	this->receiver = new std::thread(&MyClient::ReceiveStream, this);
	if (!this->receiver)
	{
		//this->last_error = 0;
		return false;
	}
	this->receiver->join();
	return true;
}

void MyClient::EndReceive()
{
	if (this->receiver)
	{
		mtx_lock.lock();
		this->is_receiving = false;
		mtx_lock.unlock();
		// wait until finish
		this->receiver->join();
		delete this->receiver;
	}

	this->receiver = NULL;
}

void MyClient::SetReceiveEvent(void(*receiveEvent)( cv::Mat& cv_img))
{
	this->receiveEvent = receiveEvent;
}

void MyClient::ReceiveStream()
{
	cv::Mat cv_img;
	cv::Mat frame_pool[STREAM_FPS];
	int frame_pool_index = 0;

	while (true)
	{
		mtx_lock.lock();
		bool thread_end = this->is_receiving;
		mtx_lock.unlock();

		// user finish
		if (!thread_end)
		{
			break;
		}
		// read frame
		if (!this->ReceiveImage(cv_img))
		{
			continue;
		}

		// event occur
		if (this->receiveEvent)
		{
			frame_pool[frame_pool_index] = cv_img.clone();
			this->receiveEvent(frame_pool[frame_pool_index]);
			frame_pool_index = (frame_pool_index + 1) % STREAM_FPS;
			//waitKey(1000 / STREAM_FPS);
			waitKey(1);
		}
	}
}
