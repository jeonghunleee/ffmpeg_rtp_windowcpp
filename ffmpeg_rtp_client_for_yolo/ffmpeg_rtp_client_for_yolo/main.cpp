﻿#include <iostream>
#include "MyClient.h"
#include "MyLogger.h"
void imgCallback(cv::Mat& cv_img);
void doAsClient();
#define F (char*)__FILE__
#define L __LINE__

int main()
{
	MyClient myClient;
	
	myClient.Initialize("127.0.0.1", 5004);
	myClient.SetReceiveEvent(imgCallback);
	myClient.StartReceive();
	//doAsClient();

}

void imgCallback(cv::Mat& cv_img) {
	resize(cv_img, cv_img, Size(640, 480), 0, 0, CV_INTER_LINEAR);
	imshow("playing", cv_img);
	//waitKey(1000 / STREAM_FPS);
}

void doAsClient() {
#if WIN32
	_putenv_s("OPENCV_FFMPEG_CAPTURE_OPTIONS", "rtsp_transport;udp");
#endif
	//VideoCapture vc = VideoCapture("http://127.0.0.1:8080/viva");
	VideoCapture vc = VideoCapture("rtp://127.0.0.1:5004");
	//VideoCapture vc = VideoCapture(0);
	//VideoCapture vc = VideoCapture("D:\F10.mp4");
#if WIN32
	_putenv_s("OPENCV_FFMPEG_CAPTURE_OPTIONS", "");
#endif


	vc.set(CV_CAP_PROP_BUFFERSIZE, 10);
	if (vc.isOpened()) {
		Mat src;
		while (vc.read(src)) {
			resize(src, src, Size(640, 480), 0, 0, CV_INTER_LINEAR);
			imshow("src", src);
			waitKey(1);
		}
	}
	return;
}