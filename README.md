***Environment***

This project has developed with VS2017.

*Project platform : x64 (Windows10) mainly at Master branch*
*Additional platform : x86_64 / amd64 ( Ubunut 16.04 LTS ) at Dev_LINUX branch*
*Certification Logic developed at Dev_TTA branch*

*Lang : C++*

**Library**

*OpenCV 3.4.0 (Must be installed in C:\)*

*Lib SWSCALE*

*pthread(not using)*

**FFMPEG Library**

*lib avcodec*
*lib avformat* 
*lib lavutil* 
*lib avdevice*

---
