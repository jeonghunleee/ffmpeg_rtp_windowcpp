
#ifndef _MY_LOGGER_H_
#define _MY_LOGGER_H_
#endif

#include <string>
#include <mutex>


void logMultiLine(char* fName, int line, const char* msg);
void printMessage(const char*);
void printMessage(int);
void printFile(char* path);
void printLogCoverLine();
