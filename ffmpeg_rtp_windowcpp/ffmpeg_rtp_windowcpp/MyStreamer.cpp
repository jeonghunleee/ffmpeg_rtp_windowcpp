#include <iostream>
#include "MyStreamer.h"
#include "MyLogger.h"

MyStreamer::MyStreamer()
	:ip("127.0.0.1"), port(5004), codec_id(AV_CODEC_ID_H264),//codec_id(AV_CODEC_ID_MPEG4),
	fmt(NULL), oc(NULL), video_st(NULL), video_is_eof(0)
{}

MyStreamer::~MyStreamer() {
	Deinitialize();
}


bool MyStreamer::Initialize(int img_width, int img_height, int64_t bit_rate,
	enum AVCodecID codec_id, std::string ip, int port)
{
	int ret;
	this->ip = ip;
	av_register_all();
	avformat_network_init();

	std::string tempUrl("");
	tempUrl.append("rtp://");
	tempUrl.append(ip + ":");
	tempUrl.append(std::to_string(port));
	//tempUrl.append("/live.sdp");

	// 맨 마지막 파라미터의 rtp url로 내보내는 context를 할당한다.
	avformat_alloc_output_context2(&this->oc, NULL, "rtp", tempUrl.c_str());
	if (!this->oc)
	{
		// ERROR
		this->last_error = MyStreamerError::CANT_ALLOC_FORMAT_CONTEXT;
		return false;
	}
	// alloc output format
	this->fmt = oc->oformat;
	if (!this->fmt)
	{
		this->last_error = MyStreamerError::CANT_ALLOC_OUTPUT_FORMAT;
		return false;
	}

	// set codec
	this->fmt->video_codec = codec_id;

	if (fmt->video_codec != AV_CODEC_ID_NONE)
		this->video_st = add_stream(oc, &video_codec, fmt->video_codec, img_width, img_height, bit_rate);

	// 비디오 코덱을 열고, 필요한 버퍼(frame)을 할당하는 함수.
	if (this->video_st) {
		open_video(this->oc, this->video_codec, this->video_st);
	}
	else {
		printf("add stream error\n");
	}

	av_dump_format(this->oc, 0, tempUrl.c_str(), 1);
	char errorBuff[80];

	if (!(fmt->flags & AVFMT_NOFILE)) {

		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		//'circular_buffer_size' option was set but it is not supported on this build (pthread support is required)
		ret = avio_open(&oc->pb, tempUrl.c_str(), AVIO_FLAG_WRITE);
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		if (ret < 0) {
			this->last_error = MyStreamerError::CANT_OPEN_RTSP_OUTPUT;
			fprintf(stderr, "Could not open outfile '%s': %s", tempUrl.c_str(), av_make_error_string(errorBuff, 80, ret));
			return false;
		}
	}

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//Using AVStream.codec to pass codec parameters to muxers is deprecated, use AVStream.codecpar instead.
	ret = avformat_write_header(oc, NULL);
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	if (ret < 0) {
		this->last_error = MyStreamerError::CANT_WRITE_HEADER;
		fprintf(stderr, "Error occurred when writing header: %s", av_make_error_string(errorBuff, 80, ret));
		return false;
	}

	return true;

}

void MyStreamer::Deinitialize() {
	// 아래 av_write_trailer는 output context를 닫기 전에 호출해야 한다는데 정확히는 몰라서 더 찾아봐야한다..
	// Write the trailer, if any. The trailer must be written before you
	// close the CodecContexts open when you wrote the header; otherwise
	// av_write_trailer() may try to use memory that was freed on
	// av_codec_close(). 
	if (oc)
		av_write_trailer(oc);

	// Close each codec. 
	if (video_st)
		close_video(video_st);
	//if (audio_st)
	//  close_audio(audio_st);

	if (fmt && oc)
		if (!(fmt->flags & AVFMT_NOFILE))
			// Close the output file. 
			avio_close(oc->pb);

	// free the stream 
	if (oc)
		avformat_free_context(oc);

	oc = NULL;
	fmt = NULL;
	video_st = NULL;
}

AVStream* MyStreamer::add_stream(AVFormatContext *oc, AVCodec **codec, enum AVCodecID codec_id,
	int img_width, int img_height, int64_t bit_rate)
{
	AVCodecContext *c;
	AVStream *st;

	// find the encoder
	*codec = avcodec_find_encoder(codec_id);
	if (!(*codec)) {
		fprintf(stderr, "Could not find encoder for '%s'\n",
			avcodec_get_name(codec_id));
		exit(1);
	}

	// output context에 대한 스트림을 생성한다.
	st = avformat_new_stream(oc, *codec);
	if (!st) {
		fprintf(stderr, "Could not allocate stream\n");
		exit(1);
	}
	st->id = oc->nb_streams - 1;
	c = st->codec;

	// 오디오 전송을 안할거면 AVMEDIA_TYPE_VIDEO만 신경쓰면 된다.
	switch ((*codec)->type) {
	case AVMEDIA_TYPE_AUDIO:
		c->sample_fmt = (*codec)->sample_fmts ?
			(*codec)->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;
		c->bit_rate = 64000;
		c->sample_rate = 44100;
		c->channels = 2;
		break;
		// st->codec에 비디오 코덱, 전송률, 이미지 해상도, FPS등을 설정한다.
	case AVMEDIA_TYPE_VIDEO:
		c->codec_id = codec_id;
		c->bit_rate = bit_rate;
		// Resolution must be a multiple of two. 
		c->width = img_width;
		c->height = img_height;
		// timebase: This is the fundamental unit of time (in seconds) in terms
		// of which frame timestamps are represented. For fixed-fps content,
		// timebase should be 1/framerate and timestamp increments should be
		// identical to 1.
		c->time_base.den = STREAM_FPS;
		c->time_base.num = 1;
		c->gop_size = 12; // emit one intra frame every twelve frames at most 
		c->pix_fmt = STREAM_PIX_FMT;
		if (c->codec_id == AV_CODEC_ID_MPEG2VIDEO) {
			// just for testing, we also add B frames
			c->max_b_frames = 2;
		}
		if (c->codec_id == AV_CODEC_ID_MPEG1VIDEO) {
			// Needed to avoid using macroblocks in which some coeffs overflow.
			// This does not happen with normal video, it just happens here as
			// the motion of the chroma plane does not match the luma plane.
			c->mb_decision = 2;
		}
		break;

	default:
		break;
	}

	// Some formats want stream headers to be separate.
	if (oc->oformat->flags & AVFMT_GLOBALHEADER)
		c->flags |= CODEC_FLAG_GLOBAL_HEADER;

	return st;
}

void MyStreamer::open_video(AVFormatContext *oc, AVCodec *codec, AVStream *st)
{
	int ret;
	AVCodecContext *c = st->codec;

	// open the codec 
	ret = avcodec_open2(c, codec, NULL);
	if (ret < 0) {
		// ERROR
		fprintf(stderr, "Could not open video codec: ");
		return;
	}

	// allocate and init a re-usable frame 
	// av_frame_alloc 함수는 frame의 데이터 버퍼를 할당하지 않는다.
	// 데이터를 제외한 나머지 부분만을 할당한다.
	this->frame = av_frame_alloc();
	if (!this->frame) {
		// ERROR
		fprintf(stderr, "Could not allocate video frame\n");
		return;
	}
	this->frame->format = c->pix_fmt;
	this->frame->width = c->width;
	this->frame->height = c->height;

	// Allocate the encoded raw picture.
	ret = avpicture_alloc(&this->dst_picture, c->pix_fmt, c->width, c->height);
	if (ret < 0) {
		fprintf(stderr, "Could not allocate picture: ");
		exit(1);
	}
	ret = avpicture_alloc(&this->src_picture, AV_PIX_FMT_BGR24, c->width, c->height);
	if (ret < 0) {
		fprintf(stderr, "Could not allocate temporary picture:");
		exit(1);
	}

	// copy data and linesize picture pointers to frame 
	// AVFrame이 AVPicture의 확장 구조체라고 할 수 있다.
	// AVFrame과 AVPicture 구조체의 정의를 보면 알 수 있겠지만, AVFrame의 처음 두개 멤버(이미지 데이터)가 AVPicture와 같다.
	// 위 주석에서 설명됐듯이 av_frame_alloc함수는 데이터 버퍼를 할당하지 않기 때문에, dst_picture에 할당된 데이터 버퍼를 frame 변수가 쓰게 하는 것이다.
	*((AVPicture *)(this->frame)) = dst_picture;
}

bool MyStreamer::StreamImage(cv::Mat cv_img, bool is_end)
{
	if (video_st && !this->video_is_eof)
	{
		write_video_frame(this->oc, this->video_st, cv_img, is_end);
		return true;
	}
	else
		return false;
}

void MyStreamer::write_video_frame(AVFormatContext *oc, AVStream *st, cv::Mat cv_img, int flush)
{
	int ret;
	static struct SwsContext *sws_ctx;
	AVCodecContext *c = st->codec;

	if (!flush) {
		// BGR opencv image to AV_PIX_FMT_YUV420P
		cv::resize(cv_img, cv_img, cv::Size(c->width, c->height));

		// BGR24 이미지를 YUV420 이미지로 변환하기 위한 context를 할당받는다.
		if (!sws_ctx) {
			sws_ctx = sws_getContext(c->width, c->height, AV_PIX_FMT_BGR24,
				c->width, c->height, c->pix_fmt,
				SWS_BICUBIC, NULL, NULL, NULL);
			if (!sws_ctx) {
				fprintf(stderr,
					"Could not initialize the conversion context\n");
				exit(1);
			}
		}

		// OpenCV Mat 데이터를 ffmpeg 이미지 데이터로 복사
		avpicture_fill(&this->src_picture, cv_img.data, AV_PIX_FMT_BGR24, c->width, c->height);

		// BGR24 이미지를 YUV420이미지로 복사
		sws_scale(sws_ctx, (const uint8_t * const *)(this->src_picture.data), this->src_picture.linesize,
			0, c->height, this->dst_picture.data, this->dst_picture.linesize);
	}

	AVPacket pkt = { 0 };
	int got_packet;
	av_init_packet(&pkt);

	// encode the image
	// 스트림의 코덱에 맞게 인코딩하여 pkt변수에 할당된다.
	this->frame->pts = this->frame_count;
	ret = avcodec_encode_video2(c, &pkt, flush ? NULL : this->frame, &got_packet);
	if (ret < 0) {
		fprintf(stderr, "Error encoding video frame:");
		exit(1);
	}
	// If size is zero, it means the image was buffered. 
	if (got_packet) {
		// 제대로 이미지가 인코딩 됐으면 스트림에 이미지를 쓴다.
		ret = write_frame(oc, &c->time_base, st, &pkt);
	}
	else {
		if (flush)
			this->video_is_eof = 1;
		ret = 0;
	}

	if (ret < 0) {
		fprintf(stderr, "Error while writing video frame: ");
		exit(1);
	}
	this->frame_count++;
}

int MyStreamer::write_frame(AVFormatContext *fmt_ctx, const AVRational *time_base, AVStream *st, AVPacket *pkt)
{
	// rescale output packet timestamp values from codec to stream timebase 
	pkt->pts = av_rescale_q_rnd(pkt->pts, *time_base, st->time_base, AVRounding(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
	pkt->dts = av_rescale_q_rnd(pkt->dts, *time_base, st->time_base, AVRounding(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
	pkt->duration = av_rescale_q(pkt->duration, *time_base, st->time_base);
	pkt->stream_index = st->index;

	// Write the compressed frame to the media file. 
	return av_interleaved_write_frame(fmt_ctx, pkt);
}

bool MyStreamer::StartStream(int numOfCamera) {
	this->is_streaming = false;
	this->video_cap.open(numOfCamera);
	if (!this->video_cap.isOpened()) {
		printf("Video open Error \n");
		return false;
	}

	//printf("%.3f\n", this->video_cap.get(CV_CAP_PROP_FPS));
	this->mtx_lock.lock();
	this->is_streaming = true;
	this->mtx_lock.unlock();
	this->sender = new std::thread(&MyStreamer::SendStream, this);
	if (!this->sender) {
		printf("thread Error!\n");
		return false;
	}
	this->sender->join();
}
bool MyStreamer::StartStream(String path) {
	this->is_streaming = false;
	printf("%s\n", path.c_str());
	this->video_cap.open(path);
	if (!this->video_cap.isOpened()) {
		printf("Video open Error \n");
		return false;
	}
	this->mtx_lock.lock();
	this->is_streaming = true;
	this->mtx_lock.unlock();
	this->sender = new std::thread(&MyStreamer::SendStream, this);
	if (!this->sender) {
		printf("thread Error!\n");
		return false;
	}
	//this->sender->join();
	return true;
}

void MyStreamer::SendStream()
{
	cv::Mat cam_img;
	cv::Mat view_img;
	logMultiLine((char*)__FILE__, __LINE__, "Thread(SendStream) Started");
	int key;

	if (!SOCKTEST) {
		this->tmessage = new std::thread(&MyStreamer::test_msg, this);
		tmessage->join();

		this->tmessage = new std::thread(&MyStreamer::test_msg, this);
	}
	while (true)
	{
		mtx_lock.lock();
		bool thread_end = this->is_streaming;
		mtx_lock.unlock();

		// end of video stream
		if (!this->video_cap.read(cam_img))
		{

			logMultiLine((char*)__FILE__, __LINE__, "End of video.");
			//EndStream();
			break;
		}

		if (!this->sender) {
			printf("thread Error!\n");
			return;
		}

		resize(cam_img, cam_img, Size(640, 480), 0, 0, CV_INTER_LINEAR);
		if (TEST) {
			rect_flag = 0;
			//mtx_lock.lock();
			for (int i = 0; i < rect_num; i++) {
				rectangle(cam_img, Rect(rect_xy[i][0], rect_xy[i][2], rect_xy[i][1] - rect_xy[i][0], rect_xy[i][3] - rect_xy[i][2]), Scalar(255, 0, 0), 4);
			}
			//mtx_lock.unlock();
			rect_flag = 1;
			imshow("playing", cam_img);
		}
		else {
			view_img = cam_img.clone();
			rect_flag = 0;
			mtx_lock.lock();
			for (int i = 0; i < rect_num; i++) {
				rectangle(view_img, Rect(rect_xy[i][0], rect_xy[i][2], rect_xy[i][1] - rect_xy[i][0], rect_xy[i][3] - rect_xy[i][2]), Scalar(255, 0, 0), 4);
			}
			mtx_lock.unlock();
			rect_flag = 1;
			imshow(video_name[video_cnt], view_img);
		}


		if (!thread_end)
		{
			// write last frame
			if (!StreamImage(cam_img, true))
			{
				return;
			}
			break;
		}
		// write frame
		if (!StreamImage(cam_img, false))
		{
			return;
		}
		// FPS만큼 기다린다.
		//std::this_thread::sleep_for(std::chrono::milliseconds(1000 / STREAM_FPS));
		key = waitKey(80);
		//if (key > 0) {
		//	if (key == 2555904) {
		//		//printf("%.3f\n", this->video_cap.get(CV_CAP_PROP_POS_FRAMES));
		//		this->video_cap.set(CV_CAP_PROP_POS_FRAMES, this->video_cap.get(CV_CAP_PROP_POS_FRAMES) + 300);
		//	}
		//	else if (key == 2424832) {
		//		//printf("%.3f\n", this->video_cap.get(CV_CAP_PROP_POS_FRAMES));
		//		this->video_cap.set(CV_CAP_PROP_POS_FRAMES, this->video_cap.get(CV_CAP_PROP_POS_FRAMES) - 300);
		//	}
		//}
	}
}

void MyStreamer::EndStream()
{
	if (sender)
	{
		/*mtx_lock.lock();
		is_streaming = false;
		mtx_lock.unlock();*/
		// wait until finish
		sender->join();
		logMultiLine((char*)__FILE__, __LINE__, "before delete");
		delete sender;
		logMultiLine((char*)__FILE__, __LINE__, "deleted");
	}
	if (video_cap.isOpened()) {
		logMultiLine((char*)__FILE__, __LINE__, "before release");
		video_cap.release();
		logMultiLine((char*)__FILE__, __LINE__, "released");
	}

	sender = NULL;

}

void MyStreamer::close_video(AVStream *st) {
	avcodec_close(st->codec);
	//std::cout << "codec" << std::endl;
	av_free(this->src_picture.data[1]);
	//std::cout << "src" << std::endl;
	av_free(this->dst_picture.data[0]);
	//std::cout << "dst" << std::endl;
	av_frame_free(&this->frame);
	//std::cout << "frame" << std::endl;
	this->video_is_eof = 0;
}

SOCKET MyStreamer::StartServer() {
	WSADATA wsaData;
	SOCKET clntSock;
	SOCKADDR_IN servAddr;

	char sAddr[20];
	int sPort = 6666;
	unsigned int Addr;

	if (MANNUALCONNECT) {
		printf("Server Address : ");
		scanf("%s", sAddr);
	}
	else {
		strcpy(sAddr, this->ip.c_str());
		printf("Server Address :%s ", sAddr);
	}

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		printf("Load Winsock 2.2 DLL Error\n");
		return -1;
	}

	clntSock = socket(AF_INET, SOCK_STREAM, 0);
	if (clntSock == INVALID_SOCKET) {
		printf("Socket Error\n");
		return -1;
	}

	memset(&servAddr, 0, sizeof(SOCKADDR_IN));
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(sPort);
	servAddr.sin_addr.s_addr = inet_addr(sAddr);

	if (connect(clntSock, (struct sockaddr*)&servAddr, sizeof(servAddr)) == SOCKET_ERROR) {
		printf("Connection Error\n");
		return -1;
	}
	else {
		printf("Connect OK!\nStart...\n");
		this->clntSock = clntSock;
	}
}

void MyStreamer::CloseServer() {
	closesocket(clntSock);
	WSACleanup();
	printf("Close Connection..\n");
}

void MyStreamer::msg() {
	char message[BUFSIZE] = { 0, };
	char receive_msg[BUFSIZE] = { 0, };
	char send_msg[BUFSIZE] = { 0, };

	while (1) {
		memset(receive_msg, 0, sizeof(receive_msg));
		memset(message, 0, BUFSIZE);
		memset(send_msg, 0, BUFSIZE);

		printf("Send Message : ");
		scanf("%s", message);

		send_msg[0] = STX;


		if (strcmp(message, "START") == 0) {
			send_msg[1] = 1;
			send_msg[2] = START;
		}
		else if (strcmp(message, "END") == 0) {
			send_msg[1] = 1;
			send_msg[2] = END;
		}
		else if (strcmp(message, "RECT") == 0) {
			send_msg[1] = 1;
			send_msg[2] = RECT;
		}
		else if (strcmp(message, "MSG") == 0) {
			send_msg[1] = strlen(message) + 1;
			send_msg[2] = MSG;
			strcat(send_msg, message);
		}
		else {
			printf("Re-enter\n");
			continue;
		}

		send_msg[strlen(send_msg)] = ETX;

		send(clntSock, send_msg, strlen(send_msg), 0);

		printf("Message Receive ...\n");

		if (send_msg[2] == START || send_msg[2] == END) {
			recv(clntSock, receive_msg, sizeof(receive_msg), 0);

			printf("%s\n", receive_msg);

			if (receive_msg[0] != STX || receive_msg[strlen(receive_msg) - 1] != ETX || receive_msg[2] != ACK) {
				printf("Receive Error\n");
				continue;
			}
			printf("Receive ACK\n");
		}
		else if (send_msg[2] == MSG) {
			recv(clntSock, receive_msg, sizeof(receive_msg), 0);

			if (receive_msg[0] != STX || receive_msg[strlen(receive_msg) - 1] != ETX || receive_msg[2] != MSG) {
				printf("Receive Error\n");
				continue;
			}
			else {
				int len = receive_msg[1];

				for (int i = 0; i < len - 1; i++) {
					printf("%c", receive_msg[i + 3]);
				}
				printf("\n");
			}
		}
		else if (send_msg[2] == RECT) {
			recv(clntSock, receive_msg, sizeof(receive_msg), 0);

			//printf("%s\n", receive_msg);
			if (receive_msg[0] != STX || receive_msg[strlen(receive_msg) - 1] != ETX || receive_msg[2] != RECT) {
				printf("Receive Error\n");
				continue;
			}
			else {
				int numOfRec = receive_msg[3] - 0x10;
				rect_num = numOfRec;

				if (numOfRec == 0)
					printf("Empty\n");
				else {
					parsh_Rect(receive_msg);
					char *token = strtok(msg_rect, ",");

					int i = 0;
					while (token != NULL) {
						rect_xy[i / 4][i % 4] = atoi(token);
						//printf("%d\n", rect_xy[i / 4][i % 4]);
						token = strtok(NULL, ",");
						i++;
					}
				}
			}
		}
	}
}

void MyStreamer::test_msg() {
	char message[10] = { 0, };
	char receive_msg[BUFSIZE] = { 0, };
	char send_msg[BUFSIZE] = { 0, };

	if (start_flag == 0) {
		strcat(message, "START");
	}
	else {
		strcat(message, "RECT");
	}

	while (1) {
		memset(receive_msg, 0, sizeof(receive_msg));
		memset(send_msg, 0, BUFSIZE);

		send_msg[0] = STX;


		if (strcmp(message, "START") == 0) {
			send_msg[1] = 1;
			send_msg[2] = START;
		}
		else if (strcmp(message, "END") == 0) {
			send_msg[1] = 1;
			send_msg[2] = END;
		}
		else if (strcmp(message, "RECT") == 0) {
			send_msg[1] = 1;
			send_msg[2] = RECT;
		}
		else if (strcmp(message, "MSG") == 0) {
			send_msg[1] = strlen(message) + 1;
			send_msg[2] = MSG;
			strcat(send_msg, message);
		}
		else {
			printf("Re-enter\n");
			continue;
		}

		send_msg[strlen(send_msg)] = ETX;

		send(clntSock, send_msg, strlen(send_msg), 0);

		//printf("Message Receive ...\n");

		if (send_msg[2] == START || send_msg[2] == END) {
			recv(clntSock, receive_msg, sizeof(receive_msg), 0);

			//printf("%s\n", receive_msg);

			if (receive_msg[0] != STX || receive_msg[strlen(receive_msg) - 1] != ETX || receive_msg[2] != ACK) {
				printf("Receive Error\n");
				continue;
			}
			printf("Receive ACK\n");
			start_flag = 1;
			return;
		}
		else if (send_msg[2] == MSG) {
			recv(clntSock, receive_msg, sizeof(receive_msg), 0);

			if (receive_msg[0] != STX || receive_msg[strlen(receive_msg) - 1] != ETX || receive_msg[2] != MSG) {
				printf("Receive Error\n");
				continue;
			}
			else {
				int len = receive_msg[1];

				for (int i = 0; i < len - 1; i++) {
					printf("%c", receive_msg[i + 3]);
				}
				printf("\n");
			}
		}
		else if (send_msg[2] == RECT) {
			recv(clntSock, receive_msg, sizeof(receive_msg), 0);
			printf("%s\n", receive_msg);
			//printf("%s\n", receive_msg);
			if (receive_msg[0] != STX || receive_msg[strlen(receive_msg) - 1] != ETX || receive_msg[2] != RECT) {
				printf("Receive Error\n");
				continue;
			}
			else if (rect_flag == 1) {
				int numOfRec = receive_msg[3] - 0x10;
				rect_num = numOfRec;

				if (numOfRec == 0) {}
				//printf("Empty\n");
				else {
					parsh_Rect(receive_msg);
					char *token = strtok(msg_rect, ",");

					int i = 0;
					while (token != NULL) {
						rect_xy[i / 4][i % 4] = atoi(token);
						//printf("%d\n", rect_xy[i / 4][i % 4]);
						token = strtok(NULL, ",");
						i++;
					}
				}
			}
		}
	}
}

void MyStreamer::parsh_Rect(char msg[]) {
	memset(msg_rect, 0, sizeof(msg_rect));
	char _ret[BUFSIZE];
	for (char i = 0; i < msg[1] - 2; i++) {
		msg_rect[i] = msg[i + 4];
	}
}

void MyStreamer::video_txt() {
	FILE *p = fopen("C:\\text.txt", "r");

	int i = 0;
	while (!feof(p)) {
		fgets(video_name[i], 20, p);
		video_name[i][strlen(video_name[i]) - 1] = 0;
		printf("%s\n", video_name[i]);
		i++;
	}
}