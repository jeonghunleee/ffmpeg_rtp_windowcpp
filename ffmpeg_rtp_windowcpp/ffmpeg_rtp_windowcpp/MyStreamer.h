#ifndef _MY_STREAMER_H_
#define _MY_STREAMER_H_
#endif

#define STREAM_FPS 30
#define STREAM_PIX_FMT  AV_PIX_FMT_YUV420P


//////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <WinSock2.h>
#pragma comment(lib,"ws2_32.lib")
#define BUFSIZE 1024
#define STX 0x02
#define ETX 0x03
#define ACK 0x04
#define START 0x05
#define END 0x06
#define RECT 0x07
#define MSG 0x08
#define TEST 0
#define SOCKTEST 0
#define MANNUALCONNECT 0
/////////////////////////////////////////

#include <string>
#include <ctime>
#include <mutex>
#include <thread>

// ffmpeg
extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libswscale/swscale.h>
#include <libavutil/time.h>
#include <libavdevice/avdevice.h>
}
#pragma comment(lib, "avcodec.lib")
#pragma comment(lib, "avformat.lib")
#pragma comment(lib, "avutil.lib")
#pragma comment(lib, "swresample.lib")
#pragma comment(lib, "swscale.lib")

#define CODEC_FLAG_GLOBAL_HEADER (1 << 22)


//opencv
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp> // Basic OpenCV structures (cv::Mat)
#include <opencv2/imgproc/imgproc.hpp>
using namespace cv;

enum MyStreamerError {
	CANT_ALLOC_FORMAT_CONTEXT = 10,
	CANT_ALLOC_OUTPUT_FORMAT = 11,
	CANT_OPEN_RTSP_OUTPUT = 12,
	CANT_WRITE_HEADER = 13,
	NO_FFMPEG_ERROR = 100
};

class MyStreamer {
public:
	MyStreamer();
	~MyStreamer();
private:
	MyStreamerError last_error;

	std::string ip;
	int port;

	int rect_num;
	int rect_xy[240][4];
	char msg_rect[BUFSIZE];
	SOCKET clntSock;

	enum AVCodecID codec_id;
	AVOutputFormat *fmt;
	AVFormatContext *oc;
	AVStream *video_st; //, *audio_st;
	AVCodec *video_codec; //, *audio_codec;
	// stream members

	// 전역 변수, 클래스 멤버변수 등으로 재사용할 버퍼를 할당해놓자 
	AVFrame* frame;
	// 아래 두개 변수는 각각 OpenCV 이미지(BGR24), ffmpeg 이미지(이 예제에선 YUV420)를 위한 변수이다.
	AVPicture src_picture, dst_picture;

	int frame_count = 0;
	int video_is_eof = 0; //, audio_is_eof;

	VideoCapture video_cap;
	bool is_streaming;
	std::mutex mtx_lock;


	AVStream* add_stream(AVFormatContext *oc, AVCodec **codec, enum AVCodecID codec_id,
		int img_width, int img_height, int64_t bit_rate);
	void open_video(AVFormatContext *oc, AVCodec *codec, AVStream *st);
	bool StreamImage(cv::Mat cv_img, bool is_end);
	void write_video_frame(AVFormatContext *oc, AVStream *st, cv::Mat cv_img, int flush);
	int write_frame(AVFormatContext *fmt_ctx, const AVRational *time_base, AVStream *st, AVPacket *pkt);
	void close_video(AVStream *video_st);
	void SendStream();

public:
	bool Initialize(int img_width, int img_height, int64_t bit_rate,
		enum AVCodecID codec_id = AV_CODEC_ID_MPEG1VIDEO,
		std::string ip = "127.0.0.1", int port = 5004);
	void Deinitialize();
	bool StartStream(int numOfCamera);
	bool StartStream(String path);
	void EndStream();
	std::thread* sender;

	SOCKET StartServer();
	void CloseServer();
	void msg();
	void test_msg();
	void parsh_Rect(char msg[]);
	std::thread* tmessage;
	int start_flag = 0;
	int rect_flag = 0;

	char *ttt[5] = { 0, };
	char video_name[5][20] = { 0, };
	void video_txt();
	int video_cnt = 0;
};