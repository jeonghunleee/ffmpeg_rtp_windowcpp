#include <iostream>
#include "MyStreamer.h"
#include "MyLogger.h"

#define BITRATE 1600000
//#define BITRATE 3500000
//using std::thread;
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@@@@@@@@@@ prototype @@@@@@@@@@@@@@@
void doAsClient();
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


//####################################
//########### FIELD ##################

//#####################################

int main(int argc, char* argv[]) {
	logMultiLine((char*)__FILE__, __LINE__, "Main start");
	MyStreamer mStreamer;

	char* IP_ADDRESS = (char*)"192.168.0.38";
	char* PATH_DIR = (char*)"C:\\독전.mp4";

	// 승수영화경로 :"C:\\양키영화.mp4"
	// 정훈영화경로 :D:\\Videos\\영화\\A.Beautiful.Mind.2001.1080p.BluRay.H264.AAC-RARBG\\A.Beautiful.Mind.2001.1080p.BluRay.H264.AAC-RARBG.mp4


	//if (!mStreamer.Initialize(640, 480, BITRATE,AV_CODEC_ID_MPEG4)) {
//	AV_CODEC_ID_H264
	//AV_CODEC_ID_MPEG1VIDEO
	if (!mStreamer.Initialize(640, 480, BITRATE, AV_CODEC_ID_MPEG1VIDEO, argc == 1 ? IP_ADDRESS : argv[1], 5004)) {
		printf("Initialize Error \n");
	}
	logMultiLine((char*)__FILE__, __LINE__, "Initialized");

	//mStreamer.StartStream(0);
	/*#if WIN32
	bool ret = mStreamer.StartStream("D:\\F10.mp4");
	#else`
	bool ret = mStreamer.StartStream("/home/marco/FFMPEG_Linux_Client/server_lib_test/test.mp4");
	#endif
	*/

	//mStreamer.video_txt();
	//mStreamer.video_cnt = 0;
	//while (mStreamer.video_name[mStreamer.video_cnt][0] != NULL) {
	//	if (!SOCKTEST) mStreamer.StartServer();
	//	bool ret = mStreamer.StartStream(mStreamer.video_name[mStreamer.video_cnt]);
	//	//mStreamer.EndStream();
	//	if (!SOCKTEST) mStreamer.CloseServer();
	//	mStreamer.video_cnt++;
	//	mStreamer.start_flag = 0;
	//}

	if (!SOCKTEST) mStreamer.StartServer();
	//bool ret = mStreamer.StartStream(argc == 1 ? PATH_DIR : argv[2]);
	bool ret = mStreamer.StartStream(0);
	waitKey(1000);
	mStreamer.EndStream();
	if (!SOCKTEST) mStreamer.CloseServer();


	//mStreamer.sender->join();

	logMultiLine((char*)__FILE__, __LINE__, "Program finish");
	return 0;
}

void doAsClient() {
	//#if WIN32
	_putenv_s("OPENCV_FFMPEG_CAPTURE_OPTIONS", "rtsp_transport;udp");
	//#endif
		//VideoCapture vc = VideoCapture("http://127.0.0.1:8080/viva");
	VideoCapture vc = VideoCapture("rtp://127.0.0.1:5004");
	//VideoCapture vc = VideoCapture(0);
	//VideoCapture vc = VideoCapture("D:\F10.mp4");
#//if WIN32
	_putenv_s("OPENCV_FFMPEG_CAPTURE_OPTIONS", "");
	//#endif


	vc.set(CV_CAP_PROP_BUFFERSIZE, 10);
	if (vc.isOpened()) {
		Mat src;
		while (vc.read(src)) {
			resize(src, src, Size(640, 480), 0, 0, CV_INTER_LINEAR);
			imshow("src", src);
			waitKey(1);
		}
	}
	return;
}