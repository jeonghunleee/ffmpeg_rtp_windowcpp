#include <iostream>
#include "MyLogger.h"
std::mutex myLog_mtx_lock;
void printLogCoverLine() {
	printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
}

void printMessage(int line) {
	printf("   line %d", line);
}
void printMessage(const char* msg) {
	printf("   %s", msg);
}
void printFile(char* path) {
	printf("   %s\n@", path);
}

void logMultiLine(char* fName, int line, const char* msg) {
	myLog_mtx_lock.lock();
	printLogCoverLine(); 
	printf("@");	printFile(fName);	printMessage(line);
	printf("\n@");  printMessage(msg); printf("\n");
	printLogCoverLine();
	myLog_mtx_lock.unlock();
}
